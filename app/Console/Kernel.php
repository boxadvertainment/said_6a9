<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->call(function () {
             \DB::table('rewards')->delete();
             $rewards = [
                 'ticket' => [ 5 => 12, 4 => 15, 2 => 25 ],
                 'montre' => [ 3 => 60 ],
                 'lunchbox' => [ 12 => 5, 9 => 6, 5 => 8],
                 'creme' => [ 10 => 6, 6 => 10, 4 => 10]
             ];
             \DB::table('rewards')->insert([
                 'name' => 'tablette',
                 'starts_from' => (new \DateTime('06:00:00'))->add(new \DateInterval('PT' . mt_rand(0, 120) . 'M'))
             ]);
             foreach ($rewards as $name => $tab) {
                 $time = new \DateTime('06:00:00');
                 foreach ($tab as $nb => $min) {
                     for ($i=0; $i < $nb ; $i++) {
                         \DB::table('rewards')->insert([
                             'name' => $name,
                             'starts_from' => $time
                         ]);
                         $time->add(new \DateInterval('PT' . $min . 'M'));
                     }
                 }
             }
         })->daily();
    }
}
