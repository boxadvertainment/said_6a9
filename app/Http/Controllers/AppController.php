<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Participant;
use App\Game;
use App\Reward;
use Carbon\Carbon;

class AppController extends Controller
{
    function __construct() {
        $this->middleware('close', ['except' => ['home']]);
    }

    public function home()
    {
        return view('home');
    }

    public function signup(Request $request)
    {
        $validator = \Validator::make( $request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:participants',
            'phone' => 'required|regex:/^[0-9]{8}$/|unique:participants',
            'cin' => 'required|regex:/^[0-9]{8}$/|unique:participants'
        ]);

        $validator->setAttributeNames([
            'name' => 'Nom et prénom',
            'email' => 'Email',
            'phone' => 'Téléphone',
        ]);

        if ( $validator->fails() ) {
            return response()->json(['success' => false, 'message' => implode('<br>', $validator->errors()->all()) ]);
        }

        $participant = session('participant');
        if ( ! $participant ) {
            return response('Votre session a été expirée, veuillez réessayer.', 500);
        }

        $participant->name = $request->input('name');
        $participant->email = $request->input('email');
        $participant->phone = $request->input('phone');
        $participant->cin = $request->input('cin');
        $participant->ip = $request->ip();

        if ( $participant->save() ) {
            session(['participantId' => $participant->id]);
            return response()->json(['success' => true ]);
        }
        return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
    }

    public function test()
    {
        dd(new \DateTime('2015-12-19'));
        $reward = Reward::where('rewarded', false)->where('starts_from', '>', new \DateTime('today'))->get();

        if ($reward->isEmpty()) {
            $reward->random();
        } else {
            echo 'adsf';
        }
        dd($reward);
        $today = Carbon::today();
        $lastPlayed = Participant::find(1)->updated_at;
        dd($today->isSameDay($lastPlayed), $lastPlayed);
    }

    public function play(Request $request)
    {
        if ( ! session('participantId') )
            return response('Votre session a été expirée, veuillez réessayer.', 500);

        if (config('app.closed')) {
            return response()->json(collect([1,4,7])->random());
        }

        $participant = Participant::find( session('participantId') );

        if ( $participant->winner || $participant->games()->where('created_at', '>', new \DateTime('today'))->first())  {
            return response()->json([ 'success' => false ]);
        }

        $game = new Game;
        $game->participant()->associate($participant);
        $game->ip = $request->ip();

        \DB::beginTransaction();
        $reward = Reward::where('rewarded', false)->where('starts_from', '<', new \DateTime())->orderBy(\DB::raw('rand()'))->lockForUpdate()->first();

        if ($reward) {
            $reward->rewarded = true;
            $participant->winner = true;
            $participant->reward = $reward->name;
            $game->reward = $reward->name;
            if ( ! $participant->save() || ! $reward->save() ) {
                \DB::rollBack();
                return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
            }
            $rewardsNumber = ['ticket' => 8, 'montre' => 3, 'lunchbox' => 5, 'creme' => 2, 'tablette' => 6];
            $response = $rewardsNumber[$reward->name];
        } else {
            $response = collect([1,4,7])->random();
        }
        \DB::commit();

        if ( $game->save() ) {
            return response()->json($response);
        }

        return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
    }
}
