<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Participant;
use App\Game;
use App\Photo;

class AdminController extends Controller
{
    public function dashboard(Request $request)
    {
        if ($request->input('username') != 'said') {
            return response('Unauthorized.', 401);
        }

        $statParticipants = Participant::select(\DB::raw('SUBSTR(created_at, 1, 10) as date'))->get()->groupBy('date');
        $statGames = Game::select(\DB::raw('SUBSTR(created_at, 1, 10) as date'))->get()->groupBy('date');
        $count = Participant::count();


        if ($request->input('date')) {
            $participants = Participant::where('winner', true)->where('updated_at', 'like', $request->input('date'). '%')->orderBy('name')->paginate(100);
        } else {
            $participants = Participant::paginate(100);
        }

        $begin = new \DateTime('2015-11-24');
        $now = new \DateTime();
        $daterange = new \DatePeriod($begin, new \DateInterval('P1D'), $now);

        $dates = [];
        foreach($daterange as $date){
            $dates[] =  $date->format("Y-m-d");
        }
        array_splice($dates, 1, 5);

        return view('admin.dashboard', ['participants' => $participants, 'count' => $count, 'dates' => $dates, 'statParticipants' => $statParticipants, 'statGames' => $statGames]);

    }

    public function stat(Request $request)
    {
        if ($request->input('username') != 'said') {
            return response('Unauthorized.', 401);
        }

        $statParticipants = Participant::select(\DB::raw('SUBSTR(created_at, 1, 10) as date'))->get()->groupBy('date');
        $statGames = Game::select(\DB::raw('SUBSTR(created_at, 1, 10) as date'))->get()->groupBy('date');
        $count = Participant::count();


        return view('admin.stat', ['count' => $count, 'statParticipants' => $statParticipants, 'statGames' => $statGames]);

    }

    public function login()
    {
        return true;
    }

}
