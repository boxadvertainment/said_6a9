<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'AppController@home');
Route::get('test', 'AppController@test');
Route::post('play', 'AppController@play');

Route::post('auth/facebookLogin/{offline?}', 'Auth\AuthController@facebookLogin');
Route::post('signup', 'AppController@signup');

/*Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);*/

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin'
], function()
{
    Route::get('/', ['uses' => 'AdminController@dashboard', 'as' => 'admin.dashboard']);
    Route::get('stat', ['uses' => 'AdminController@stat', 'as' => 'admin.stat']);
});
