<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $now = new \DateTime();
        $dateClosed = new \DateTime('2016-12-29');
        $dateOpened = new \DateTime('2015-11-30');

        $closed = false;
        if ($now < $dateOpened || $now > $dateClosed) {
            $closed = true;
        }
        config(['app.closed' => $closed]);
        view()->share('closed', $closed);

        $startTime = new \DateTime('06:00:00');
        $endTime = new \DateTime('23:00:00');

        $suspended = false;
        if ( $now < $startTime || $now > $endTime) {
            $suspended = true;
        }
        config(['app.suspended' => $suspended]);
        view()->share('suspended', $suspended);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
