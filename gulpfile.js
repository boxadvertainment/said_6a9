var elixir   = require('laravel-elixir');

require('./elixir-extensions');

elixir(function(mix) {
    mix
        .sass('main.scss', 'public/css/main.css')
        .babel([
          'facebookUtils.js',
          'main.js'
        ], 'public/js/main.js')
        .scripts([
            // bower:js
            'jquery/dist/jquery.js',
            'bootstrap-sass/assets/javascripts/bootstrap.js',
            'sweetalert/dist/sweetalert.min.js',
            // endbower
            '/gsap/src/uncompressed/easing/EasePack.js',
            '/gsap/src/uncompressed/plugins/CSSPlugin.js',
            '/gsap/src/uncompressed/TweenLite.js',
        ], 'public/js/vendor.js', 'vendor/bower_components' )
        .styles([
            // bower:css
            'sweetalert/dist/sweetalert.css',
            // endbower
            '/font-awesome/css/font-awesome.css',
        ], 'public/css/vendor.css', 'vendor/bower_components')
        // .copy('vendor/bower_components/modernizr/src/Modernizr.js', 'public/js/modernizr.js')
        .copy('vendor/bower_components/font-awesome/fonts', 'public/fonts')
        .wiredep({
            exclude: ['TweenMax']
        })
        .browserSync({
            proxy: process.env.APP_URL
        });

        if (elixir.config.production) {
            mix
                .images()
                .copy('public/fonts', 'public/build/fonts')
                .version(['css/main.css', 'css/vendor.css', 'js/main.js', 'js/vendor.js']);
        }
});
