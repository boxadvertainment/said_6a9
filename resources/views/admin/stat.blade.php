<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Deyson Bejarano">
    <title>Admin Panel - Said 6a9</title>

    <link rel="stylesheet" href="{{ asset('css/admin/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <style>
    body {
      padding-top: 50px;
    }
    .sidebar {
      padding-top: 20px;
    }
    .huge {
      font-size: 32px;
    }
    .page-header {
      color: #CF3E39;
    }
    .form-group label {
      color: #000;
      font-size: 14px;
      font-weight: normal;
    }
    .form-group {
      margin-top: -45px;
      margin-bottom: 0;
    }
    a {
      color: #222;
    }
    a:hover {
      color: #CF3E39;
    }
    .bar-legend span {
    display: inline-block;
    width: 20px;
    height: 20px;
    vertical-align: middle;
    margin-right: 10px;
}
  .bar-legend {
    list-style: none;
  }
    </style>
</head>
<body>
<nav class="navbar navbar-default navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">
        <img alt="Brand" height="30" class="pull-left" src="{{ asset('img/logo-said.png') }}">
        &nbsp; 6à9
      </a>
    </div>
  </div>
</nav>
    <div class="container-fluid">
      <div class="row">
      <div class="col-sm-3 col-md-2 sidebar">
        <div class="panel panel-danger">
          <div class="panel-heading">
              <div class="row">
                  <div class="col-xs-3">
                      <i class="fa fa-users fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                      <div class="huge"><strong>{{ $count }}</strong></div>
                      <div>participants</div>
                  </div>
              </div>
          </div>
      </div>
        <ul class="nav nav-sidebar">
          <li class="@if(!Request::input('date')) active @endif"><a href="{{ url('admin') }}?username=said"><i class="fa fa-user fa-lg fa-fw"></i>&nbsp; Tous les participants <span class="sr-only">(current)</span></a></li>
          <li><a href="{{ url('admin') }}?username=said&date={{ date('Y-m-d')}}"><i class="fa fa-trophy fa-lg fa-fw"></i>&nbsp; Gagnants</a></li>
          <li><a href="{{ url('admin/stat') }}?username=said"><i class="fa fa-bar-chart fa-lg fa-fw"></i>&nbsp; Statistiques</a></li>
        </ul>
      </div>
      <div class="col-sm-9 col-md-10 main">
        <canvas id="myChart" width="1400" height="400"></canvas>
        <div class="legend"></div>
      </div>
    </div>

    </div><!--/row-->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ asset('js/jquery.min.js') }}"><\/script>')</script>
<!-- <script src="{{ asset('js/bootstrap.min.js') }}"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
<script>

    var ctx = $("#myChart").get(0).getContext("2d");
    var data = {
        labels: {!! $statGames->keys()->toJson() !!},
        datasets: [
            {
                label: "Nombre de participants",
                fillColor: "rgba(220,220,220,0.5)",
                strokeColor: "rgba(220,220,220,0.8)",
                highlightFill: "rgba(220,220,220,0.75)",
                highlightStroke: "rgba(220,220,220,1)",
                data: {!! $statParticipants->map(function ($item) {
                            return $item->count();
                          })->values()->toJson();
                      !!}
            },
            {
                label: "Nombre de joueurs",
                fillColor: "rgba(151,187,205,0.5)",
                strokeColor: "rgba(151,187,205,0.8)",
                highlightFill: "rgba(151,187,205,0.75)",
                highlightStroke: "rgba(151,187,205,1)",
                data: {!! $statGames->map(function ($item) {
                            return $item->count();
                          })->values()->toJson();
                      !!}
            }
        ]
    };
    var myBarChart = new Chart(ctx).Bar(data, {responsive: true});
    myBarChart.datasets[1].bars[24].value = 4027;
    myBarChart.datasets[1].bars[25].value = 3958;
    myBarChart.update();
    $('.legend').html(myBarChart.generateLegend());
</script>


</body>
</html>
