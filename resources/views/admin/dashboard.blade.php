<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Deyson Bejarano">
    <title>Admin Panel - Said 6a9</title>

    <link rel="stylesheet" href="{{ asset('css/admin/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <style>
    body {
      padding-top: 50px;
    }
    .sidebar {
      padding-top: 20px;
    }
    .huge {
      font-size: 32px;
    }
    .page-header {
      color: #CF3E39;
    }
    .form-group label {
      color: #000;
      font-size: 14px;
      font-weight: normal;
    }
    .form-group {
      margin-top: -45px;
      margin-bottom: 0;
    }
    a {
      color: #222;
    }
    a:hover {
      color: #CF3E39;
    }
    </style>
</head>
<body>
<nav class="navbar navbar-default navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">
        <img alt="Brand" height="30" class="pull-left" src="{{ asset('img/logo-said.png') }}">
        &nbsp; 6à9
      </a>
    </div>
  </div>
</nav>
    <div class="container-fluid">
      <div class="row">
      <div class="col-sm-3 col-md-2 sidebar">
        <div class="panel panel-danger">
          <div class="panel-heading">
              <div class="row">
                  <div class="col-xs-3">
                      <i class="fa fa-users fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                      <div class="huge"><strong>{{ $count }}</strong></div>
                      <div>participants</div>
                  </div>
              </div>
          </div>
      </div>
        <ul class="nav nav-sidebar">
          <li class="@if(!Request::input('date')) active @endif"><a href="{{ url('admin') }}?username=said"><i class="fa fa-user fa-lg fa-fw"></i>&nbsp; Tous les participants <span class="sr-only">(current)</span></a></li>
          <li><a href="{{ url('admin') }}?username=said&date={{ date('Y-m-d')}}"><i class="fa fa-trophy fa-lg fa-fw"></i>&nbsp; Gagnants</a></li>
          <li><a href="{{ url('admin/stat') }}?username=said"><i class="fa fa-bar-chart fa-lg fa-fw"></i>&nbsp; Statistiques</a></li>
        </ul>
      </div>
      <div class="col-sm-9 col-md-10 main">
        <h1 class="page-header">
          @if (Request::input('date'))
            <div class="row">
              <div class="col-md-9">Gagnants</div>
              <div class="col-md-3">
                <form method="get">
                  <div class="form-group">
                    <label for="date">Séléctionner une date</label>
                    <select id="date" name="date" class="form-control">
                      @foreach($dates as $date)
                        <option value="{{ $date }}" @if($date == Request::input('date')) selected @endif>{{ ($date == date('Y-m-d')) ? 'Aujourd\'hui' : $date }}</option>
                      @endforeach
                    </select>
                  </div>
                  <input type="hidden" name="username" value="said">
                </form>
              </div>
            </div>
          @else
            Participants
          @endif
        </h1>

        <div class="table-responsive">
          <table class="table table-striped">
              <thead>
                  <th>#</th>
                  <th>Nom et prénom</th>
                  <th>email</th>
                  <th>Téléphone</th>
                  <th>CIN</th>
                  @if(Request::input('date'))
                  <th>Cadeau</th>
                  @endif
                  <th>Date de création</th>
              </thead>
              <tbody>
              @foreach($participants as $key => $participant)
                  <tr>
                      <td>{{ ($participants->currentPage() - 1) * 100 + $key + 1}}</td>
                      <td><a href="https://www.facebook.com/{{ $participant->facebook_id }}" target="_blank">
                          <img src="http://graph.facebook.com/{{ $participant->facebook_id }}/picture?width=100" class="img-circle" width="30px" height="30px" alt=""/> {{ $participant->name . ' ' .  $participant->lastname}}</a></td>
                      <td>{{ $participant->email }}</td>
                      <td>{{ $participant->phone }}</td>
                      <td>{{ $participant->cin }}</td>
                      @if(Request::input('date'))
                      <td>{{ $participant->reward }}</td>
                      @endif
                      <td>{{ $participant->created_at }}</td>
                  </tr>
              @endforeach
              </tbody>
          </table>
        </div>

        <div class="text-center">
          @if (Request::input('date'))
            {!! $participants->appends(['username' => 'said', 'date' => Request::input('date')])->render() !!}
          @else
            {!! $participants->appends(['username' => 'said'])->render() !!}
          @endif
        </div>
      </div>
    </div>

    </div><!--/row-->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ asset('js/jquery.min.js') }}"><\/script>')</script>
<!-- <script src="{{ asset('js/bootstrap.min.js') }}"></script> -->
<script>
    $('#date').change(function(e) {
      $(this).parents('form').submit();
      if ($(this).val()) {
        console.log($(this).val());

      }
    });
</script>

</body>
</html>
