<!doctype html>
<!--[if lt IE 9]>      <html class="no-js lt-ie9" lang="{{ App::getLocale() }}"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="{{ App::getLocale() }}"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title'){{ Config::get('app.name') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <meta name="author" content="box.agency" /> -->


    @include('partials.socialMetaTags', [
        'title' => 'Réveillez vous de bonne humeur avec Saïd',
        'description' => 'Chaque matin,  de 6H à 9H, tentez de gagner pleins de cadeaux avec Saïd'
    ])


    <!-- Metta CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Use RealFaviconGenerator.net to generate favicons  -->
    @include('partials.favicon')

    @if( App::environment('production') )
        <link rel="stylesheet" href="{{ asset(elixir('css/vendor.css')) }}">
        <link rel="stylesheet" href="{{ asset(elixir('css/main.css')) }}">
    @else
        <link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
        <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    @endif
    @yield('styles')

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        BASE_URL    = '{{ url('/') }}';
        CURRENT_URL = '{{ Request::url() }}';
        FB_APP_ID   = '{{ config('services.facebook.client_id') }}';
    </script>

    <script src="{{ asset('js/modernizr.js') }}"></script>
</head>
<body class="@yield('class')">
    <!--[if lt IE 9]>
    <div class="alert alert-dismissible outdated-browser show" role="alert">
        <h6>Votre navigateur est obsolète !</h6>
        <p>Pour afficher correctement ce site et bénéficier d'une expérience optimale, nous vous recommandons de mettre à jour votre navigateur.
            <a href="http://outdatedbrowser.com/fr" class="update-btn" target="_blank">Mettre à jour maintenant </a>
        </p>
        <a href="#" class="close-btn" title="Fermer" data-dismiss="alert" aria-label="Fermer">&times;</a>
    </div>
    <![endif]-->

    <div class="loader">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <main class="main" role="main">
        @yield('content')
    </main>

    <div class="modal fade video-modal" >
      <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="{{ asset('img/exit-modal.png') }}" alt=""></button>
            <iframe width="100%" height="325" src="" frameborder="0" allowfullscreen></iframe>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    @yield('body')

    @if( App::environment('production') )
        <script src="{{ asset(elixir('js/vendor.js')) }}"></script>
        <script src="{{ asset(elixir('js/main.js')) }}"></script>
    @else
        <script src="{{ asset('js/vendor.js') }}"></script>
        <script src="{{ asset('js/main.js') }}"></script>
    @endif
    @yield('scripts')

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','//connect.facebook.net/en_US/fbevents.js');

    fbq('init', '416242491912498');
    fbq('track', "PageView");
    fbq('track', 'ViewContent');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=416242491912498&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->


    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-69863373-1', 'auto');
      ga('send', 'pageview');

    </script>
</body>
</html>
