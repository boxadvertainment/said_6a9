@extends('layout')

@section('class', 'home')

@section('content')
{{ $suspended .' - '. $closed }}
    <div class="container">
        <div class="ui-top">
            <div class="logo pull-left"></div>
            <div class="social pull-right hidden-xs hidden-sm">
                <a href="https://www.facebook.com/Said.Chocolats/" class="sn" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="https://www.youtube.com/channel/UCyr6iwGSeqWVUPP5HNrkWOA" class="sn" target="_blank"><i class="fa fa-youtube"></i></a>
                <a href="#" class="share"> Partagez </a>
                <a href="#" class="sound active">
                    <i class="fa fa-volume-off off"></i>
                    <i class="fa fa-volume-up on"></i>
                </a>
            </div>
        </div>

        <div class="pages-wrapper clearfix">
            @if( $closed )
                <img src="{{ asset('img/logo-6a9.png') }}" class="logo-6a9 closed" />

                <div class="page closed-page row">
                    <div class="content">
                        <a href="https://www.facebook.com/Said.Chocolats" class="fb-page-link"><img src="{{ asset('img/fb-link.png') }}" alt=""></a>
                        <img src="{{ asset('img/closed.png') }}" alt="">
                        <div class="actions">
                            <button type="button" class="video video1" name="button" data-video="0m4XqVS4K_o" data-toggle="modal" data-target=".video-modal"></button>
                            <button type="button" class="video video2" name="button" data-video="GB90zjJO_dA" data-toggle="modal" data-target=".video-modal"></button>
                        </div>
                    </div>
                </div>
                <img src="{{ asset('img/logo-6a9.png') }}" class="logo-6a9 suspended" />

                <div class="page landing-page hide suspended">
                    <h2><img src="{{ asset('img/title-supended.png') }}" alt="" class="hidden-xs"></h2>
                    <div class="overvideo" style="width: 100%; height: 100%; position: absolute;">
                        <iframe id="player" width="100%" height="325" src="https://www.youtube.com/embed/0m4XqVS4K_o?enablejsapi=1&rel=0" frameborder="0" allowfullscreen class="video-susp"></iframe>
                    </div>

                    <div class="els">
                        <img src="{{ asset('img/goutes.png') }}" alt="" class="goutes hidden-xs hidden-sm">
                        <img src="{{ asset('img/chocoline.png') }}" alt="" class="chocoline hidden-xs hidden-sm">
                        <img src="{{ asset('img/choco.png') }}" alt="" class="choco hidden-xs hidden-sm">
                        <img src="{{ asset('img/tartine.png') }}" alt="" class="tartine hidden-xs hidden-sm">
                        <img src="{{ asset('img/mini-tarti.png') }}" alt="" class="mini-tarti hidden-xs hidden-sm">
                    </div>
                    <div class="actions">
                         <div class="clearfix"></div>
                         <p style="width: 80% ;">Le jeu est cloturé, Rejoignez nous sur <a href="https://www.facebook.com/Said.Chocolats/" style="color: #EAD073;" target="_blank">notre page Facebook</a>. #KounSaid</p>
                     </div>
                </div>
            @elseif( $suspended )
                <img src="{{ asset('img/logo-6a9.png') }}" class="logo-6a9 suspended" />

                <div class="page landing-page hide suspended">
                    <h2><img src="{{ asset('img/title-supended.png') }}" alt="" class="hidden-xs"></h2>
                    <div class="overvideo" style="width: 100%; height: 100%; position: absolute;">
                        <iframe id="player" width="100%" height="325" src="https://www.youtube.com/embed/0m4XqVS4K_o?enablejsapi=1&rel=0" frameborder="0" allowfullscreen class="video-susp"></iframe>
                    </div>

                    <div class="els">
                        <img src="{{ asset('img/goutes.png') }}" alt="" class="goutes hidden-xs hidden-sm">
                        <img src="{{ asset('img/chocoline.png') }}" alt="" class="chocoline hidden-xs hidden-sm">
                        <img src="{{ asset('img/choco.png') }}" alt="" class="choco hidden-xs hidden-sm">
                        <img src="{{ asset('img/tartine.png') }}" alt="" class="tartine hidden-xs hidden-sm">
                        <img src="{{ asset('img/mini-tarti.png') }}" alt="" class="mini-tarti hidden-xs hidden-sm">
                    </div>

                   <div class="actions">
                        <div class="clearfix"></div>
                        <p>Connectez-vous de <span>6h à 9h</span> pour un réveil Saïd <br> Plein de cadeaux à gagner tous les jours</p>
                    </div>

                </div>
            @else
                <img src="{{ asset('img/logo-6a9.png') }}" class="logo-6a9" />
                <div class="page landing-page hide">

                    <div class="els">
                        <img src="{{ asset('img/goutes.png') }}" alt="" class="goutes hidden-xs hidden-sm">
                        <img src="{{ asset('img/chocoline.png') }}" alt="" class="chocoline hidden-xs hidden-sm">
                        <img src="{{ asset('img/choco.png') }}" alt="" class="choco hidden-xs hidden-sm">
                        <img src="{{ asset('img/tartine.png') }}" alt="" class="tartine hidden-xs hidden-sm">
                        <img src="{{ asset('img/mini-tarti.png') }}" alt="" class="mini-tarti hidden-xs hidden-sm">
                    </div>

                    <div class="actions">
                        <button type="button" class="video video1 hidden-xs" name="button" data-video="0m4XqVS4K_o" data-toggle="modal" data-target=".video-modal"></button>
                        <button type="button" class="start-btn login-btn" name="button">Participez</button>
                        <div class="clearfix visible-xs"></div>
                        <button type="button" class="video video1 visible-xs" name="button" data-video="0m4XqVS4K_o" data-toggle="modal" data-target=".video-modal"></button>
                        <button type="button" class="video video2" name="button" data-video="GB90zjJO_dA" data-toggle="modal" data-target=".video-modal"></button>
                        <div class="clearfix"></div>
                        <p>Connectez-vous de <span>6h à 9h</span> pour un réveil Saïd <br> Plein de cadeaux à gagner tous les jours</p>
                    </div>

                </div>
                <!-- /END Landing page -->

                <div class="page signup-page hide">
                    <div class="form-wrapper">
                        <div class="content clearfix">
                            <form id="signup-form" action="{{ action('AppController@signup') }}" method="post" accept-charset="utf-8">
                                {!! csrf_field() !!}
                                <h2 class="form-title">Inscription <span>remplir le formulaire</span></h2>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" id="name" name="name" class="form-control" placeholder="Nom et prénom *" required title="Ce champ est obligatoire">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="tel" name="phone" class="form-control" placeholder="Téléphone *" maxlength="8" required pattern="^[2549][0-9]{7}$" title="Téléphone incorrect">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="email" id="email" name="email" class="form-control" placeholder="Email *" required  title="Email incorrect">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="test" class="form-control" name="cin" placeholder="CIN *" required maxlength="8" pattern="^[0-9]{8}$" title="CIN incorrect">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="info"><small>* Champ obligatoire</small><br> En cliquant sur "Je valide", vous acceptez le <a href="reglement.pdf" target="_blank">règlement du jeu</a></p>
                                        <button type="submit" class="submit-btn">
                                            <span class="pull-left">Je valide</span>
                                            <i class="icon-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <!-- /END Signup page -->


                <div class="page game-page hide">
                    <div class="game-wrapper">
                        <div class="spin-wrapper">
                            <img src="{{ asset('img/pointer.png') }}" class="pointer" alt="" >
                            <img src="{{ asset('img/spin-btn.png') }}" id="spin-btn" alt="" >
                            <img src="{{ asset('img/over-spin.png') }}" class="over-spin" alt="" >
                            <img src="{{ asset('img/spin.png') }}" id="spin" alt="" />
                        </div>
                    </div>
                </div>
                <!-- /END Game page -->

                <div class="page win-page hide">
                    <div class="content">
                        <div class="gift"><img src="{{ asset('img/gifts/gift-1.png') }}" alt="" id="gifts"></div>
                        <img src="{{ asset('img/win.png') }}" alt="Vous avez gagné" class="bg-win" />
                        <div class="actions">
                            <button type="button" class="video video1" name="button" data-video="0m4XqVS4K_o" data-toggle="modal" data-target=".video-modal"></button>
                            <button type="button" class="video video2" name="button" data-video="GB90zjJO_dA" data-toggle="modal" data-target=".video-modal"></button>
                        </div>
                    </div>

                </div>
                <!-- /END Win page -->

                <div class="page lose-page hide">
                    <div class="content">
                    <img src="{{ asset('img/lose.png') }}" alt="Vous avez perdu" />
                        <div class="actions">
                            <button type="button" class="video video1" name="button" data-video="0m4XqVS4K_o" data-toggle="modal" data-target=".video-modal"></button>
                            <button type="button" class="video video2" name="button" data-video="GB90zjJO_dA" data-toggle="modal" data-target=".video-modal"></button>
                        </div>
                    </div>

                </div>
                <!-- /END Lose page -->

            </div>
        @endif

        <div class="ui-bottom">
            <div class="slogon"></div>
            <a href="{{ asset('reglement.pdf') }}" class="cdj pull-right" target="_blank"> Règlement </a>
        </div>
    </div>
@endsection
